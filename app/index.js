'use strict'

const debug = require('debug')
const { factory: commandsFactory } = require('./commands')
const TelegramBot = require('tgfancy')
const GooglePlacesApiService = require('./services/google-places-api')

const factory = config => {
  const bot = new TelegramBot(config.telegram.API_TOKEN, {
    polling: {
      autoStart: false
    }
  })

  const services = {
    googlePlacesApi: new GooglePlacesApiService(config)
  }

  const commands = commandsFactory(bot, services)

  bot.onText(/\/start/, commands.start)

  bot.on('location', commands.location)

  return {
    start: () => {
      bot.startPolling()
      bot.getMe()
         .then(me => {
           debug('location-bot:main')(`Listening on ${me.username}`)
         })
    }
  }
}

module.exports = { factory }
