'use strict'

const factory = (bot, services) => {
  return {
    start: require('./start').factory(bot, services),
    location: require('./location').factory(bot, services)
  }
}

module.exports = { factory }
