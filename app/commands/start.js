'use strict'

const MSG = {
  SEND_YOUR_LOCATION: 'Para prosseguir, preciso da sua localzação!'
}

const factory = bot => msg => {
  bot.sendMessage(msg.chat.id, MSG.SEND_YOUR_LOCATION, {
    reply_markup: {
      keyboard: [[{
        text: 'Enviar localização',
        request_location: true
      }]],
      resize_keyboard: true
    }
  })
}

module.exports = { factory }
