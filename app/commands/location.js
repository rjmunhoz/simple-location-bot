'use strict'

const factory = (bot, services) => async msg => {
  const address = await services.googlePlacesApi.getAddress(msg.location)

  const message = address
    ? `Seu endereço é: ${address}`
    : 'Endereço não encontrado'

  bot.sendMessage(msg.chat.id, message, {
    reply_markup: {
      remove_keyboard: true
    }
  })
}

module.exports = { factory }
