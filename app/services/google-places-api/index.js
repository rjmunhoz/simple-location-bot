'use strict'

const axios = require('axios')

class GooglePlacesApiService {
  constructor (config) {
    this.$http = axios.create({
      baseURL: 'https://maps.googleapis.com/maps/api',
      params: {
        key: config.google.places.API_TOKEN
      }
    })
  }

  async getAddress ({ latitude, longitude }) {
    const { data: { results } } = await this.$http.get('/geocode/json', {
      params: {
        latlng: `${latitude},${longitude}`
      }
    })

    if (results.length) {
      return results[0].formatted_address
    }

    return null
  }
}

module.exports = GooglePlacesApiService
